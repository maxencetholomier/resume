all : clean mrproper convert

convert : 
	pdflatex -interaction=nonstopmode resume.tex

mrproper : clean
	find . -name "*.pdf" -exec rm {} +

clean : 
	find . -name "*.log" -exec rm {} +
	find . -name "*.out" -exec rm {} +
	find . -name "*.aux" -exec rm {} +
