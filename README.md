# Resume

## History 

I used to spent a lot of time formatting my resume with Microsoft Word.

It wasn't efficent and the final result perfectible.

ModernCV allow me to focus on text only.

Feel free to copy my resume and fill it with your information !

NB: **due to security reason I've removed my personnal information.**


## Installation

Pull the code : 

```bash
git clone https://gitlab.com/maxencetholomier/resume
```
Change the pictures and the informations as desired.

Then compile : 

```bash
make all
```

# Overview

![](cover.png)
